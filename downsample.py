from typing import Dict, Callable

import os
import cv2
import numpy as np

from tqdm import tqdm
from skimage import io
from scipy import ndimage
from collections import defaultdict

from constants import _C


class BaseImageIO(object):
    def __init__(self, imageSrc: str, imageDst: str, isMask: bool = False, split: Dict = {}):
        assert os.path.exists(imageSrc)
        filenames = next(os.walk(imageSrc))[2]
        assert len(filenames) > 0

        self.isMask = isMask

        self.imageSrc = imageSrc
        self.imageDst = imageDst
        if not os.path.exists(self.imageDst):
            os.makedirs(self.imageDst)

        self.toProcess = {}
        for filename in tqdm(filenames, desc='[ INFO ] Loading images...'):
            if isMask:
                x = io.imread(os.path.join(imageSrc, filename))
                x = np.expand_dims(x, axis=2)
                toTile = np.zeros((x.shape[0], x.shape[1], 1))
                self.toProcess[filename] = np.concatenate((x, toTile, toTile), axis=2).astype(np.uint8)
            else:
                self.toProcess[filename] = io.imread(os.path.join(imageSrc, filename))

        self.toSave = {}
        self.split = split
        self.cache = dict(
            train=[],
            val=[],
            test=[]
        )

    def save(self, parentDir):
        assert len(self.toSave) > 0
        for fname, img in tqdm(self.toSave.items(), desc='[ SAVE ] Saving...'):
            basename = fname.split('.')[0]
            x = ''
            if self.isMask:
                if not fname.endswith('.png'):
                    fname = basename + '.png'
                img = self.binarize(img)
            else:
                if not fname.endswith('.jpg'):
                    fname = basename + '.jpg'

            if bool(self.split):
                if basename in self.split['train']:
                    x = 'train'
                elif basename in self.split['val']:
                    x = 'val'
                elif basename in self.split['test']:
                    x = 'test'
                else:
                    x = 'unlisted'

                currDst = os.path.join(self.imageDst, parentDir, x)
            else:
                currDst = os.path.join(self.imageDst, parentDir)
            os.makedirs(currDst, exist_ok=True)

            currPath = os.path.join(currDst, fname)
            if bool(x) and x is not 'unlisted' and not self.isMask:
                self.cache[x].append(os.path.abspath(currPath))
            io.imsave(currPath, img, check_contrast=False)

        if all([bool(self.cache['train']), bool(self.cache['val']), bool(self.cache['test'])]) and not self.isMask:
            self.saveNewTxtSplits(os.path.dirname(currDst))

    def binarize(self, msk):
        assert isinstance(msk, np.ndarray)
        lessThanMsk = msk[:, :, 0] < 128
        greaterThanMsk = msk[:, :, 0] >= 128
        msk[:, :, 0][lessThanMsk] = 0
        msk[:, :, 0][greaterThanMsk] = 255
        return msk

    def saveNewTxtSplits(self, pathDst: str):
        print('[ SAVE ] .txt file for training and inference...')
        for x in ['train', 'val', 'test']:
            print(f"         Saved as {os.path.join(pathDst, f'{x}.txt')}...")
            with open(os.path.join(pathDst, f'{x}.txt'), 'w') as f:
                for i, path in enumerate(self.cache[f'{x}'], 1):
                    f.write(path+'\n') if i != len(self.cache[f'{x}']) else f.write(path)


class SplineResampler(BaseImageIO):
    def __init__(self, imageSrc: str, imageDst: str,
                 downsampler: Callable = ndimage.interpolation.zoom,
                 isMask: bool = False,
                 split: Dict = {}):
        assert callable(downsampler)
        super().__init__(imageSrc, imageDst, isMask, split)
        self.resampler = downsampler

    def __call__(self, order: int, scale: float):
        assert order >= 0
        outputSizeFactor = (scale, scale, 1)
        for fname, img in tqdm(self.toProcess.items(), desc='[ INFO ] Resampling...'):
            self.toSave[fname] = self.resampler(img, outputSizeFactor, order=order).astype(np.uint8)
        self.save(_C.SPLINE_FOLDERS[f'order_{order}'])


class KernelResampler(BaseImageIO):
    def __init__(self, imageSrc: str, imageDst: str,
                 downsampler: Callable = cv2.resize,
                 isMask: bool = False,
                 split: Dict = {}):
        assert callable(downsampler)
        super().__init__(imageSrc, imageDst, isMask, split)
        self.resampler = downsampler

    def __call__(self, kernel: int, scale: float):
        assert kernel in [cv2.INTER_NEAREST, cv2.INTER_LANCZOS4]
        for fname, img in tqdm(self.toProcess.items(), desc='[ INFO ] Resampling...'):
            outputSize = (int(img.shape[0]*scale), int(img.shape[1]*scale))
            self.toSave[fname] = self.resampler(img, outputSize, interpolation=kernel)
        self.save(_C.KERNEL_FOLDERS[f'kernel_{kernel}'])


class CopyVanillaData(BaseImageIO):
    def __init__(self, imageSrc: str, imageDst: str,
                 isMask: bool = False,
                 split: Dict = {}):
        super().__init__(imageSrc, imageDst, isMask, split)
        self.copy = lambda x : x

    def __call__(self):
        for fname, img in tqdm(self.toProcess.items(), desc='[ INFO ] Copying...'):
            self.toSave[fname] = self.copy(img)
        self.save('from_sk01')
