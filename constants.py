import os

class Constants:
    def __init__(self):
        self.SCALE = 50/80

        self.ORDERS = [0, 5]
        self.KERNELS = [0, 4]

        self.IMG_SRCS = [
            '../data/satellite_slices/HKT_updated_slicing_outputs',
            '../data/satellite_slices/SPD_slicing_outputs',
            '../data/satellite_slices/FKD_slicing_outputs'
        ]

        self.MSK_SRCS = [
            '../data/satellite_masks/Fukuoka_Dome/easy_harder',
            '../data/satellite_masks/Fukuoka-Hakata/Parkings/easy_harder',
            '../data/satellite_masks/Sapporo_Dome/labels_SPD_easy_hard'
        ]

        self.SPLIT_TXT_FILES = dict(
            train='../sk_deeplab/datasets_combined/train.txt',
            val='../sk_deeplab/datasets_combined/val.txt',
            test='../sk_deeplab/datasets_combined/test.txt'
        )
        self.makeSplit()

        self.SPLINE_FOLDERS = dict(
            order_0='resampled_spline_0',
            order_1='resampled_spline_1',
            order_2='resampled_spline_2',
            order_3='resampled_spline_3',
            order_4='resampled_spline_4',
            order_5='resampled_spline_5'
        )

        self.KERNEL_FOLDERS = dict(
            kernel_0='resampled_nn',
            kernel_4='resampled_lanczos'
        )

    def makeSplit(self):
        assert os.path.exists(self.SPLIT_TXT_FILES['train'])
        assert os.path.exists(self.SPLIT_TXT_FILES['val'])
        assert os.path.exists(self.SPLIT_TXT_FILES['test'])

        with open(self.SPLIT_TXT_FILES['train'], 'r') as f:
            lines = f.read()
        trainBasenames = lines.split('\n')[:-1]

        with open(self.SPLIT_TXT_FILES['val'], 'r') as f:
            lines = f.read()
        valBasenames = lines.split('\n')[:-1]

        with open(self.SPLIT_TXT_FILES['test'], 'r') as f:
            lines = f.read()
        testBasenames = lines.split('\n')[:-1]

        self.SPLIT = dict(
            train=trainBasenames,
            val=valBasenames,
            test=testBasenames
        )

    def makeSplineFolders(self):
        for k, v in self.SPLINE_FOLDERS.items():
            os.makedirs(v, exist_ok=True)

    def makeKernelFolders(self):
        for k, v in self.KERNEL_FOLDERS.items():
            os.makedirs(v, exist_ok=True)


_C = Constants()

