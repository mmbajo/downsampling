from typing import List, Dict, Callable

import os
import cv2
import numpy as np

from tqdm import tqdm
from skimage import io
from scipy import ndimage
from downsample import CopyVanillaData, SplineResampler, KernelResampler
from constants import _C

import logging
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logger = logging.getLogger('__name__')

def main():
    logger.info('[ INIT ] Using spline intrerpolation for downsampling...')
    logger.info('[ INFO ] Downsampling images...')
    for imgSrc in _C.IMG_SRCS:
        logger.info(f'[ INFO ] Processing {imgSrc} ...')
        spline = SplineResampler(imageSrc=imgSrc,
                                 imageDst='spline',
                                 isMask=False,
                                 split=_C.SPLIT)
        for order in _C.ORDERS:
            spline(order=order, scale=_C.SCALE)

    logger.info('[ INFO ] Downsampling segmentation masks...')
    for mskSrc in _C.MSK_SRCS:
        logger.info(f'[ INFO ] Processing {mskSrc} ...')
        spline = SplineResampler(imageSrc=mskSrc,
                                 imageDst='spline',
                                 isMask=True,
                                 split=_C.SPLIT)
        for order in _C.ORDERS:
            spline(order=order, scale=_C.SCALE)

    logger.info('[ INIT ] Using kernel intrerpolation for downsampling...')
    logger.info('[ INFO ] Downsampling images...')
    for imgSrc in _C.IMG_SRCS:
        logger.info(f'[ INFO ] Processing {imgSrc} ...')
        kernel = KernelResampler(imageSrc=imgSrc,
                                 imageDst='kernel',
                                 isMask=False,
                                 split=_C.SPLIT)
        for k in _C.KERNELS:
            kernel(kernel=k, scale=_C.SCALE)

    logger.info('[ INFO ] Downsampling segmentation masks...')
    for mskSrc in _C.MSK_SRCS:
        logger.info(f'[ INFO ] Processing {mskSrc} ...')
        kernel = KernelResampler(imageSrc=mskSrc,
                                 imageDst='kernel',
                                 isMask=True,
                                 split=_C.SPLIT)
        for k in _C.KERNELS:
            kernel(kernel=k, scale=_C.SCALE)

    logger.info('[ INFO ] Copying vanilla data...')
    for imgSrc in _C.IMG_SRCS:
        logger.info(f'[ INFO ] Processing {imgSrc} ...')
        copy = CopyVanillaData(imageSrc=imgSrc,
                               imageDst='vanilla',
                               isMask=False,
                               split=_C.SPLIT)
        copy()

    for mskSrc in _C.MSK_SRCS:
        logger.info(f'[ INFO ] Processing {mskSrc} ...')
        copy = CopyVanillaData(imageSrc=mskSrc,
                               imageDst='vanilla',
                               isMask=True,
                               split=_C.SPLIT)
        copy()


if __name__ == '__main__':
    main()